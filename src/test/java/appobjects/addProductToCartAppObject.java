package appobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class addProductToCartAppObject {
	private WebDriver driver;

	public addProductToCartAppObject(WebDriver driver) {
		this.driver = driver;
	}	
	public WebElement addToCart() {
		this.driver.findElement(By.xpath("//*[text()='Add to cart']"));
		Actions actions = new Actions(driver);
		actions.moveToElement(this.driver.findElement(By.xpath("//*[text()='Add to cart']")));
		actions.perform();
		this.driver.findElement(By.xpath("//*[text()='Add to cart']")).click();

		return this.driver.findElement(By.id("center_column"));
	}
	public WebElement total() {
		return this.driver.findElement(By.id("//*[@id=\"total_product_price_3_13_0\"]"));
	}
	public WebElement proceedToCheckout() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"center_column\"]/p[2]/a[1]/span[1]")));
		return element;
	}
	public WebElement productNameCheck() {
		return this.driver.findElement(By.xpath("//*[@id=\"product_3_13_0_719828\"]/td[2]/p[1]/a[1]"));
	}
	public WebElement proceedToCheckoutSummary() {
		return this.driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/p[2]/a[1]"));
	}
}